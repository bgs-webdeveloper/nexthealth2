import axios from 'axios'
import { apiURL } from "@/config";

export default {
  data: () => ({
    typeId: 1,
    included: [],
    pdfs: [],
    /*
      digitalInfoImage: null,
      digitalInfoImageError: false,
      digitalInfoImageUpdate: null,
    */
    productPDFError: false
  }),
  methods: {
    validateProduct() {
      let errorFlag = false;
      this.errors = [];
      if (!this.product.name) {
        errorFlag = true;
        this.product.nameError = true;
      }
      if (!this.product.sku_regular || this.product.sku_regular.length != 3) {
        errorFlag = true;
        this.product.skuRegularError = true;
      }

      if (
        !this.product.sku_subscription ||
        this.product.sku_subscription.length != 3
      ) {
        errorFlag = true;
        this.product.skuSubscriptionError = true;
      }

      // Oneliner
      if (!this.product.oneliner) {
        errorFlag = true;
        this.product.onelinerError = true;
      }

      // Volume
      if (!this.product.volume) {
        errorFlag = true;
        this.product.volumeError = true;
      }

      // Category
      if (this.category.length == 0 && this.updateCategory.length == 0) {
        errorFlag = true;
        this.categoryError = true;
      }

      // Bundles
      if (this.bundles.length == 0 && this.updateBundle.length == 0) {
        errorFlag = true;
        this.bundleError = true;
      }

      // Include
      if (this.include.length == 0 && this.updateInclude.length == 0) {
        errorFlag = true;
        this.includeError = true;
      }

      // Hashtags
      if (this.hashtags.length == 0 && this.updateHashtag.length == 0) {
        errorFlag = true;
        this.hashtagsError = true;
      }

      // Regular price
      if (
        !this.product.regular_price ||
        !this.isPrice(this.product.regular_price)
      ) {
        errorFlag = true;
        this.product.regularPriceError = true;
      }

      // Regular sales price
      if (
        this.product.regular_sale_price &&
        !this.isPrice(this.product.regular_sale_price)
      ) {
        errorFlag = true;
        this.product.regularSalePriceError = true;
      }

      // Cost price
      if (!this.product.cost_price || !this.isPrice(this.product.cost_price)) {
        errorFlag = true;
        this.product.costPriceError = true;
      }

      /* if (!this.product.high_level_overview_title) {
        errorFlag = true;
        this.product.highLevelOverviewTitleError = true;
      }

      if (!this.product.high_level_overview) {
        errorFlag = true;
        this.product.highLevelOverviewError = true;
      }

      if (!this.product.detailed_overview) {
        errorFlag = true;
        this.product.detailedOverviewError = true;
      }

      if (
        this.productImages.length == 0 &&
        this.productImagesUpdate.length == 0
      ) {
        errorFlag = true;
        this.productImageError = true;
      } */

      // Is digital product
      if (this.isDigital) {
        // PDFS
        if (
          this.productPDF &&
          !this.productPDF.length &&
          !this.productPDFUpdate
        ) {
          errorFlag = true;
          this.productPDFError = true;
        }
      }

      // Is not digital product
      /* if (!this.isDigital) {
        // Property
        if (this.productProperty.length == 0) {
          errorFlag = true;
          this.productPropertyError = true;
        }
        // Nutrition
        if (!this.nutritionInfoImage || !this.nutritionInfoImage.media_id) {
          this.nutritionInfoImageError = true;
          errorFlag = true;
        }
        // Additional media type VIDEO
        if (this.product.additional_media_type == "video") {
          if (this.additionalVideoMedia && this.additionalVideoMedia.length) {
            let ob = JSON.parse(this.additionalVideoMedia.video.data);
            if (
              !ob["video_title"] ||
              !ob["video_description"] ||
              !ob["video_link"] ||
              !this.additionalVideoMedia.preview.media_id
            ) {
              errorFlag = true;
              this.additionalVideoMediaError = true;
            }
          } else {
            errorFlag = true;
            this.additionalVideoMediaError = true;
          }
        }

        // Additional media type IMAGE
        if (
          this.product.additional_media_type == "image" &&
          (!this.additionalImageMedia || !this.additionalImageMedia.media_id)
        ) {
          errorFlag = true;
          this.additionalImageMediaError = true;
        }
      } */

     // debugger

      // Video overview media type
      /* if (this.product.video_overview_media_type == "video") {
        if (
          this.videoOverviewVideoMedia &&
          this.videoOverviewVideoMedia.length != 0 &&
          this.videoOverviewVideoMedia.video &&
          this.videoOverviewVideoMedia.video.data
        ) {
          let ob = this.videoOverviewVideoMedia.video.data;
          if (
            !ob["video_title"] ||
            !ob["video_description"] ||
            !ob["video_link"] ||
            !this.videoOverviewVideoMedia.preview.media_id
          ) {
            errorFlag = true;
            this.videoOverviewVideoMediaError = true;
          }
        } else {
          errorFlag = true;
          this.videoOverviewVideoMediaError = true;
        }
      } */

      /* if (
        this.product.video_overview_media_type == "image" &&
        (!this.videoOverviewImageMedia ||
          !this.videoOverviewImageMedia.media_id)
      ) {
        errorFlag = true;
        this.videoOverviewImageMediaError = true;
      } */

      if (errorFlag) {
        this.errors.push("Some fields are not filled correctly. Product cannot be created.");
      }
    },
    productDigitalError() {
      console.log('1')
    },
    changeDigitalMedia(data) {
      this.isChangeProduct = true;
      this.isProductUpdate = false;
      this.digitalInfoImageUpdate = data;
    },
    onProductType(data) {
      const typeProduct = data.create[0]
      this.typeId = Number(typeProduct)

      // 2 - this is digital type
      if (this.typeId === 2) {
        this.product.volume = 1
        this.product.units_of_mass_id = 2
      }

      this.isChangeProduct = true;
      this.updateProductType = data;
      this.isProductUpdate = false;
    },
    changeProductImage: function (data) {
      this.isChangeProduct = true;
      this.isProductUpdate = false;
      this.productImagesUpdate = data;
    },
    onChangeIncluded(data) {
      this.isChangeProduct = true;
      this.isProductUpdate = false;
      this.updateInclude = data;
    },
    updateProductOption(productId, type, data, callback) {
      if (!type) return
      axios({
        method: 'put',
        url: apiURL + '/api/admin/product/' + productId + '/' + type,
        headers: {
          Authorization: 'Bearer ' + this.token,
        },
        data: data
      }).then(response => {
        callback(response.data);
      });
    },
  },
  computed: {
    isPhysical() {
      return this.typeId === 1
    },
    isDigital() {
      return this.typeId === 2
    },
    isDigitalAndPhysical() {
      return this.typeId === 3
    }
  }
}