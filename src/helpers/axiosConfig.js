import getCookie from '@/helpers/getCookie'
import axios from 'axios'
export default () => {
    const token = getCookie('user_token')
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}