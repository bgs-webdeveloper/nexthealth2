import {
  apiURL
} from "@/config";
import store from '@/store';
import {
  LOGIN_FACEBOOK,
  LOGIN_GOOGLE
} from '@/constants/api.routes'
import router from "@/router";


const windowName = "newindow";
const authWindowOptions = "height=800,width=800,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,directories=no, status=no";

const urls = {
  'facebook': apiURL + LOGIN_FACEBOOK,
  'google': apiURL + LOGIN_GOOGLE
}

const authListener = (event) => {
  const {
    token
  } = event.detail;

  if (token) {
    store.commit('account/setToken', {
      authToken: token
    });
    router.push({
      name: 'Dashboard'
    });
  }
}
window.addEventListener("token", authListener);

export const openAuthDialog = (service) => {
  const url = urls[service];
  if (!url) return console.warn('Bad auth method name');

  let w = window.open(
    url,
    windowName,
    authWindowOptions
  );
}
