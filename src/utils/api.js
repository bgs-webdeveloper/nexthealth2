import axios from 'axios';
import { apiURL } from '@/config';
import store from '@/store';
import { AUTH_REFRESH } from '@/constants/api.routes';

const api = axios.create({
  baseURL: apiURL,
  withCredentials: false  
});

api.interceptors.response.use(
  res => {
    return res;
  },
  err => {
    console.error(err);
    if (401 === err.response.status) {
      store.dispatch('account/clearToken');
    } else {
      return Promise.reject(err);
    }
  }
);

const token = localStorage.getItem('token');
console.log(token);
if (token) {
  api.defaults.headers.common['token'] = `${token}`;
}

export default api;
